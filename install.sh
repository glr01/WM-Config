#!/usr/bin/env bash

download_source() {
  git=$(pacman -Qqen | grep 'git')
  [ -z "$git" ] && sudo pacman -S git

  git clone https://github.com/rasteli/Dotfiles.git "$HOME/Dotfiles"
}

install_fonts() {
  fonts_dir="/usr/share/fonts"

  sudo mkdir -p "$fonts_dir/ttf"
  sudo mkdir -p "$fonts_dir/otf"

  cd "$HOME/Dotfiles"
  sudo mv fonts/Font Awesome /usr/share/fonts/otf
  sudo mv fonts/* /usr/share/fonts/ttf
}

install_dotfiles() {
  cd "$HOME/Dotfiles/dotfiles"

  ls -pA | grep -v '/'\
    | xargs -I {} mv {} "$HOME/{}"

  mv .xmonad "$HOME/.xmonad"
  mv .config/* "$HOME/.config"

  [ -d "$HOME/.local" ] || mkdir -p "$HOME/.local/bin"
  mv .local/bin/* "$HOME/.local/bin"
}

install_yay() {
  sudo pacman -S --needed git base-devel
  git clone https://aur.archlinux.org/yay-bin.git
  cd yay-bin
  makepkg -si
}

install_packages() {
  cd "$HOME/Dotfiles"

  sudo pacman -S --needed - < native-packages.txt

  install_yay

  yay -S --needed - < foreign-packages.txt
  sh -c "$(curl -fsSL https://starship.rs/install.sh)"
}

install_nvim_plugins() {
  cd "$HOME/Dotfiles"
  mkdir -p "$HOME/.vim/plugged"

  git clone https://github.com/VundleVim/Vundle.vim.git "$HOME/.vim/bundle/Vundle.vim"
  nvim +PluginInstall +qall

  mv vim-themes/vim/* "$HOME/.vim/plugged/vim/autoload"
  mv vim-themes/lightline/* "$HOME/.vim/plugged/lightline.vim/autoload/lightline/colorscheme"
}

main() {
  download_source
  install_fonts
  install_dotfiles
  install_packages
  install_vim_plugins

  echo "INSTALLATION COMPLETE!"
  exit 0
}

user=$(whoami)
[ "$user" != "root" ] && main

echo "Please, run this script as a normal user!"
exit 1
