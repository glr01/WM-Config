## Scripts

If you choose to use dmenu as your launcher of the menu scripts, note that they are dependent of dmenu patched for line height support.\
You can patch it yourself or you can grab [my own build of dmenu](https://gitlab.com/glr01/glr-dmenu).

---
